# ElasticRequestBundle

[![https://img.shields.io/badge/Status-unstable-red.svg?style=flat](https://img.shields.io/badge/Status-unstable-red.svg?style=flat)](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle)
[![https://img.shields.io/badge/PHP-^7.1-blue.svg?style=flat](https://img.shields.io/badge/PHP-^7.1-blue.svg?style=flat)](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle)
[![https://img.shields.io/badge/Symfony-^3.4-blue.svg?style=flat](https://img.shields.io/badge/Symfony-^3.4-blue.svg?style=flat)](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle)
[![https://img.shields.io/badge/Licence-MIT-green.svg?style=flat](https://img.shields.io/badge/Licence-MIT-green.svg?style=flat)](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle)

## Installation

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require ibanfirst/elasticrequestbundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new IbanFirst\ElasticRequestBundle\IbanFirstElasticRequestBundle(),
        ];

        // ...
    }

    // ...
}
```

## Documentation

You can find the full documentation on the bundle [here](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle/blob/master/Resources/docs/index.md).

## Licence

This bundle is under the `MIT` Licencing; you can find the licence file [here](https://gitlab.com/ibanfirst-it/public-vendors/elasticrequestbundle/blob/master/LICENCE).